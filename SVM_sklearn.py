# Import the modules
from sklearn.externals import joblib
from sklearn import datasets
from sklearn.svm import LinearSVC
from sklearn.datasets import fetch_openml
import numpy as np
import sys

X, y = fetch_openml('mnist_784', version=1, return_X_y=True)
X = X / 255.

# rescale the data, use the traditional train/test split
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]

clf = LinearSVC()

clf.fit(X_train, y_train)
print("Training set score: %f" % clf.score(X_train, y_train))
print("Test set score: %f" % clf.score(X_test, y_test))
joblib.dump(clf, "digits_cls.pkl", compress=3)
