from pyspark.ml.linalg import Vectors
from pyspark.ml.linalg import SparseVector
from pyspark.ml.linalg import Vector
from pyspark.ml.classification import MultilayerPerceptronClassifier
from pyspark.ml.feature import StringIndexer
from pyspark.ml import Pipeline
from pyspark.ml import PipelineModel
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('multinomial-logistic-regression').getOrCreate()

trainingDF = spark.read.format("libsvm").load("mnist").cache()
testDF = spark.read.format("libsvm").load("mnist.t").cache()

def make780(v):
  return Vectors.sparse(780, v.indices, v.values)

# Define UDF
from pyspark.sql.functions import udf
from pyspark.ml.linalg import VectorUDT

udf_make780 = udf(make780, VectorUDT())

testFixedDF = testDF.select("label", udf_make780("features").alias("features"))

indexer = StringIndexer().setInputCol("label").setOutputCol("indexedLabel")

# Set the label column
mlp = MultilayerPerceptronClassifier().setLabelCol("indexedLabel")

mlp.setLayers([780, 100, 100, 10]) # 780 inputs, two hidden layers (20 and 10 neurons, respectively) and output layer of 10 neurons

pipeline = Pipeline().setStages([indexer, mlp])


model = pipeline.fit(trainingDF)

model.save("tmp/NeuralNetPipeline.parquet")

# testing
savedModel = PipelineModel.load("tmp/NeuralNetPipeline.parquet")

evaluator = MulticlassClassificationEvaluator().setLabelCol("indexedLabel")

predictions = savedModel.transform(testFixedDF)

evaluator.setMetricName("accuracy")
accuracy = evaluator.evaluate(predictions)
print("Test Error = %g" % (1.0 - accuracy))